﻿//-----------------------------------------------------------------------
// <author> 
//    Gautam Kumar
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="SchemaStub.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Schema;

    /// <summary>
    /// schema stub for implementing ISchema
    /// </summary>
    public class SchemaStub : ISchema
    {
        /// <summary>
        /// Dummy function for Decoding
        /// </summary>
        /// <param name="data">Dummy data for decoding</param>
        /// <param name="partialDecoding">partial decoding</param>
        /// <returns>succes of the decoding</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            IDictionary<string, string> dictionary = new Dictionary<string, string>
            {
                { "Msg", "Success" },
                { "fromIP", "127.0.0.1" },
                { "toIP", "127.0.0.1" },
                { "dateTime", "2:30" }
            };
            if (data == "dummyMessage")
            {
                dictionary.Add("flag", "False");
            }
            else
            {
                dictionary.Add("flag", "True");
            }
            
            return dictionary;
        }

        /// <summary>
        /// This function is used for dummy encoding
        /// </summary>
        /// <param name="tagDict">dummy dictionary for input</param>
        /// <returns>dummy return</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {
            return "Success";
        }
    }
}
