﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Create a text box to display messages. 
// </summary>
// <copyright file="ServerCreateTextBox.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Creates message text box.
        /// </summary>
        /// <param name="isClientMessage">Check if it is client message or server message<see cref="bool"/></param>
        /// <param name="tabPageNextTopHeight">Y-location of the message text box<see cref="int"/></param>
        /// <param name="messagePage">The client message page<see cref="TabPage"/></param>
        /// <param name="message">Message<see cref="string"/></param>
        /// <param name="splitContainer">The split container<see cref="SplitContainer"/></param>
        /// <param name="isSuccess">Success status<see cref="bool"/></param>
        /// <returns>The y location of the message text box<see cref="string"/></returns>
        private string CreateMessageTextBox(bool isClientMessage, int tabPageNextTopHeight, TabPage messagePage, string message, SplitContainer splitContainer, bool isSuccess)
        {
            RichTextBox messageBox = new RichTextBox();

            //// Setting properties for text box.
            messageBox.Font = new Font("Lucinda Console", 12);
            float adjustedWidth = (float)0.7 * splitContainer.Panel2.Width;
            float shiftedWidth = splitContainer.Panel2.Width - (float)(0.7 * splitContainer.Panel2.Width);
            messageBox.Width = (int)adjustedWidth;
            messageBox.Height = 30;
            messageBox.ReadOnly = true;
            messageBox.ContentsResized += new ContentsResizedEventHandler(TextBoxContentResized);

            //// Scroll to top and then after adding text box scroll to previous location.
            int prevVerticalLocation = splitContainer.Panel2.VerticalScroll.Value;
            int prevHorizontalLocation = splitContainer.Panel2.HorizontalScroll.Value;

            splitContainer.Panel2.VerticalScroll.Value = 0;
            splitContainer.Panel2.HorizontalScroll.Value = 0;
            splitContainer.Panel2.SizeChanged += new EventHandler(PanelSizeChanged);

            if (!isClientMessage && isSuccess) // Server message sent successfully.
            {
                messageBox.Location = new Point((int)shiftedWidth, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Blue;
            }
            else if (!isClientMessage && !isSuccess) // Server message sent unsuccessfully.
            {
                messageBox.Location = new Point((int)shiftedWidth, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Red;
            }
            else if (isClientMessage) // Client message received.
            {
                messageBox.Location = new Point(0, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Orange;
            }

            messageBox.AppendText(message);
            messageBox.AppendText(System.Environment.NewLine);
            messageBox.Select(0, message.Length);
            messageBox.SelectionAlignment = HorizontalAlignment.Left;

            int prevLength = messageBox.TextLength;

            string time = GetCurrentDateTime();

            if (isSuccess) // Append time for successful message.
            {
                messageBox.AppendText(time);
                messageBox.Select(prevLength, time.Length);
            }
            else // Append error message.
            {
                string errMessage = "Message not delivered";
                messageBox.AppendText(errMessage);
                messageBox.Select(prevLength, errMessage.Length);
            }

            messageBox.SelectionFont = new Font("Lucinda Console", 8);
            messageBox.SelectionColor = Color.Black;
            messageBox.SelectionAlignment = HorizontalAlignment.Right;

            splitContainer.Panel2.Controls.Add(messageBox);

            messagePage.Controls.Add(splitContainer);
            tabPageNextTopHeight += messageBox.Height + 5;

            splitContainer.Panel2.VerticalScroll.Value = prevVerticalLocation;
            splitContainer.Panel2.HorizontalScroll.Value = prevHorizontalLocation;

            return tabPageNextTopHeight.ToString(CultureInfo.InvariantCulture);
        }
    }
}
