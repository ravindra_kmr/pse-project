﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Handle form size change and on paint functions for side aligned tabs.
// </summary>
// <copyright file="ServerChatScreen.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Panel size changed.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void PanelSizeChanged(object sender, EventArgs e)
        {
            TabPage activeTab = ServerChatSectionTabs.SelectedTab;
            string clientIP = GetClientName(activeTab.Name);
            SplitContainer splitContainer = GetControl(activeTab.Controls, clientIP + "split") as SplitContainer;

            foreach (Control c in splitContainer.Controls)
            {
                float adjustedWidth = (float)0.7 * splitContainer.Panel2.Width;
                float shiftedWidth = splitContainer.Panel2.Width - (float)0.7 * splitContainer.Panel2.Width;

                foreach (Control rtb in c.Controls)
                {
                    //// Change the x location and width of text box based on the new panel size.
                    if (rtb is RichTextBox)
                    {
                        if (rtb.Location.X != 0)
                        {
                            rtb.Location = new Point((int)shiftedWidth, rtb.Location.Y);
                        }
                        rtb.Width = (int)adjustedWidth;
                    }
                }
            }
        }

        /// <summary>
        /// Resizes the text box based on message contained.
        /// </summary>
        /// <param name="Sender">The Sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ContentsResizedEventArgs"/></param>
        private void TextBoxContentResized(object Sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)Sender).Height = e.NewRectangle.Height + 5;
        }

        /// <summary>
        /// Side align and paint tabs.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="DrawItemEventArgs"/></param>
        private void AlignAndPaintTabs(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;

            if (e.Index >= ServerChatSectionTabs.TabPages.Count)
            {
                return;
            }

            TabPage tabPage = ServerChatSectionTabs.TabPages[e.Index];
            tabPage.BorderStyle = BorderStyle.None;

            Rectangle tabBounds = ServerChatSectionTabs.GetTabRect(e.Index);
            Rectangle unreadMessageCountBounds = new Rectangle(180, tabBounds.Y + 28, 30, 30);

            Brush textBrush = new SolidBrush(Color.FromArgb(32, 29, 58));

            //// Styles for inactive tabs. 
            Brush gray = new SolidBrush(Color.FromArgb(245, 245, 245));
            Font tabFont = new Font("Segoe UI", (float)15.0, FontStyle.Regular, GraphicsUnit.Pixel);

            //// Styles for active tabs.
            Brush graySelected = new SolidBrush(Color.FromArgb(225, 225, 225));
            Font tabFontActive = new Font("Segoe UI", (float)15.0, FontStyle.Bold, GraphicsUnit.Pixel);

            Font unreadMessageFont = new Font("Segoe UI", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            StringFormat stringFlags = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            string clientIP = GetClientName(tabPage.Name);

            TextBox unreadMessageCountTextBox = GetControl(tabPage.Controls, clientIP + "unreadMessage") as TextBox;
            string unreadMessageCount = unreadMessageCountTextBox.Text;

            //// Active tab.
            if (e.State == DrawItemState.Selected)
            {
                g.FillRectangle(graySelected, e.Bounds);
                unreadMessageCountTextBox.Text = "0"; // Reset unread message count.
                g.DrawString(tabPage.Text, tabFontActive, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString("0", unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
            //// Inactive tab.
            else
            {
                g.FillRectangle(gray, e.Bounds);
                g.DrawString(tabPage.Text, tabFont, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString(unreadMessageCount, unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
        }
    }
}
