﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Find UI Controls
// </summary>
// <copyright file="ServerGetUIControls.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Get child controls based on name from the parent.
        /// </summary>
        /// <param name="controlCollection">The collection of controls<see cref="Control.ControlCollection"/></param>
        /// <param name="controlName">The control name<see cref="string"/></param>
        /// <returns>The control<see cref="Control"/></returns>
        private static Control GetControl(Control.ControlCollection controlCollection, string controlName)
        {
            foreach (Control control in controlCollection)
            {
                if (control.Name.Equals(controlName, StringComparison.Ordinal)) // Control found.
                {
                    return control;
                }
            }

            return null; // No control found.
        }

        /// <summary>
        /// Gets tab page from the tab control.
        /// </summary>
        /// <param name="tabPageName">Tab page name<see cref="string"/></param>
        /// <returns>The tab page <see cref="TabPage"/></returns>
        private TabPage GetTabPage(string tabPageName)
        {
            TabControl.TabPageCollection pages = ServerChatSectionTabs.TabPages;
            foreach (TabPage page in pages)
            {
                if (page.Name.Equals(tabPageName, StringComparison.Ordinal)) // Tab page found.
                {
                    return page;
                }
            }
            return null; // Tab page not found. 
        }
    }
}
