﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Maintenance of unread message count.
// </summary>
// <copyright file="ServerCreateUIControls.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Creates a control to save tab page next y location.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <returns>The tab page next y location<see cref="TextBox"/></returns>
        private static TextBox CreateTabPageTopTextBox(string clientIP)
        {
            TextBox tabPageTop = new TextBox();

            //// Setting properties for text box.
            tabPageTop.Height = 100;
            tabPageTop.Width = 50;
            tabPageTop.Name = clientIP + "tabPageTop";
            tabPageTop.Text = "0";
            tabPageTop.Visible = false;

            return tabPageTop;
        }

        /// <summary>
        /// Updates unread message count.
        /// </summary>
        /// <param name="unreadMessageCountTextBox">The unread message count text box<see cref="TextBox"/></param>
        /// <param name="isClientMessage">Checks if it is client message or server message<see cref="bool"/></param>
        private static void UpdateUnreadMessageCount(TextBox unreadMessageCountTextBox, bool isClientMessage)
        {
            string unreadMessageCount = unreadMessageCountTextBox.Text;
            int count = int.Parse(unreadMessageCount, CultureInfo.InvariantCulture);
            if (isClientMessage)
            {
                count += 1;
            }

            unreadMessageCountTextBox.Text = count.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Creates message page.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <returns>The created tab page<see cref="TabPage"/></returns>
        private TabPage CreateMessagePage(string clientIP)
        {
            TabPage messagePage = new TabPage();

            //// Setting properties for tab page.
            messagePage.Name = clientIP + "tabPage";
            messagePage.Text = this.userNameIPMap[clientIP];
            messagePage.AutoScroll = true;
            messagePage.HorizontalScroll.Enabled = false;
            messagePage.VerticalScroll.Enabled = true;
            messagePage.Size = new Size(ServerChatSectionTabs.Width, ServerChatSectionTabs.Height);

            TextBox unreadMessageCountTextBox = this.CreateUnreadMessageCountTextBox(clientIP);
            TextBox tabPageTop = CreateTabPageTopTextBox(clientIP);

            messagePage.Controls.Add(unreadMessageCountTextBox);
            messagePage.Controls.Add(tabPageTop);

            return messagePage;
        }

        /// <summary>
        /// Creates unread message count text box.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <returns>The text box<see cref="TextBox"/></returns>
        private TextBox CreateUnreadMessageCountTextBox(string clientIP)
        {
            TextBox unreadMessageCountTextBox = new TextBox();

            //// Setting properties for text box.
            unreadMessageCountTextBox.Name = clientIP + "unreadMessage";
            unreadMessageCountTextBox.Height = this.ServerChatSectionTabs.Height;
            unreadMessageCountTextBox.Width = this.ServerChatSectionTabs.Width;
            unreadMessageCountTextBox.Visible = false;
            unreadMessageCountTextBox.Text = "00";

            return unreadMessageCountTextBox;
        }
    }
}
